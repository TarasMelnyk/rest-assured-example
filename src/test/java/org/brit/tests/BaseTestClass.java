package org.brit.tests;

import io.restassured.RestAssured;

/**
 * Created by sbryt on 8/26/2016.
 */
public class BaseTestClass {
    protected String PET_ENDPOINT = "/pet";
    protected String STORE_ENDPOINT = "/store";
    protected String USER_ENDPOINT = "/user";


    public BaseTestClass() {
        RestAssured.baseURI = "http://petstore.swagger.io";
        RestAssured.basePath = "/v2";
    }


}
