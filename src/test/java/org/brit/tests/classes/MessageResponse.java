package org.brit.tests.classes;

/**
 * Created by sbryt on 9/9/2016.
 */

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageResponse {
    private Integer code;
    private String type;
    private String message;
}