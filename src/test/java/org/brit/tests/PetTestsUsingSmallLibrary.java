package org.brit.tests;

import org.apache.commons.lang3.RandomStringUtils;
import org.brit.tests.actions.pets.PetsActions;
import org.brit.tests.classes.Pet;
import org.brit.tests.classes.StatusEnum;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by sbryt on 9/9/2016.
 */
public class PetTestsUsingSmallLibrary {
    private PetsActions petsActions;
    private Pet pet;

    @BeforeClass
    public void beforeClass() {
        petsActions = new PetsActions();
        pet = new Pet();
    }

    @Test(dependsOnMethods = "addNewPet")
    public void getPetsByStatus() {
        List<Pet> pets = petsActions.getPetsByStatus(StatusEnum.available);
        for (Pet pet : pets) {
            Assert.assertEquals(pet.getStatus(), StatusEnum.available);
        }
    }

    @Test
    public void addNewPet() {
        Pet petResponse = petsActions.addNewPet(pet);
        Assert.assertEquals(pet, petResponse);
    }

    @Test(dependsOnMethods = "getPetsByStatus")
    public void updatePet() {
        String newName = "NewName_" + RandomStringUtils.randomAlphanumeric(5);
        pet.setName(newName);
        pet = petsActions.updatePet(pet);
        Assert.assertEquals(newName, pet.getName());
    }


    @Test(dependsOnMethods = "updatePet")
    public void deletePetItem() {
        petsActions.deletePet(pet);
        Assert.assertTrue(!petsActions.isPetExists(pet));
    }
}
