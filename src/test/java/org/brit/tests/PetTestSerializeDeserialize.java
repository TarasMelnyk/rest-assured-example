package org.brit.tests;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.RandomStringUtils;
import org.brit.tests.classes.Pet;
import org.brit.tests.classes.StatusEnum;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static io.restassured.RestAssured.given;

/**
 * Created by sbryt on 9/1/2016.
 */
public class PetTestSerializeDeserialize extends BaseTestClass {
    private String petId = RandomStringUtils.randomNumeric(10);
    private String petName = "MyLittlePet_" + RandomStringUtils.randomAlphanumeric(5);

    private Pet pet;


    private RequestSpecification requestSpecification = new RequestSpecBuilder()
            .addHeader("api_key", Authentication.getApiKey())
            .setContentType(ContentType.JSON)
            .log(LogDetail.ALL).build();

    @Test(dependsOnMethods = "addNewPet")
    public void getPetsByStatus() {
        List<Pet> pets = given(requestSpecification)
                .queryParam("status", StatusEnum.available.toString())
                .get(PET_ENDPOINT + "/findByStatus")
                .then().log().all()
                .extract().body()
                .jsonPath().getList("", Pet.class);
        for (Pet pet : pets) {
            Assert.assertEquals(pet.getStatus(), StatusEnum.available);
        }
    }

    @Test(dependsOnMethods = "getPetsByStatus")
    public void getPetByIdAndDoCheck() {
        Assert.assertEquals(
                given(requestSpecification)
                        .pathParam("petId", pet.getId())
                        .get(PET_ENDPOINT + "/{petId}").as(Pet.class), pet);
    }

    @Test
    public void addNewPet() {
        Pet petRequest = Pet.builder()
                .id(Long.parseLong(petId))
                .name(petName)
                .status(StatusEnum.available)
                .build();

        pet = given(requestSpecification)
                .body(petRequest)
                .post(PET_ENDPOINT).as(Pet.class);

        Assert.assertEquals(petRequest, pet);

        List<Pet> pets = given(requestSpecification)
                .queryParam("status", StatusEnum.available.toString())
                .get(PET_ENDPOINT + "/findByStatus")
                .then().log().all()
                .extract().body()
                .jsonPath().getList("", Pet.class);
        Assert.assertTrue(pets.contains(pet));
    }

    @Test(dependsOnMethods = "updateExistingPet")
    public void deletePetItem() {
        given(requestSpecification)
                .pathParam("petId", pet.getId())
                .delete(PET_ENDPOINT + "/{petId}");

        List pets = given(requestSpecification)
                .queryParam("status", StatusEnum.available.toString())
                .get(PET_ENDPOINT + "/findByStatus")
                .then().log().all()
                .extract().body()
                .jsonPath().getList("", Pet.class);

        Assert.assertTrue(!pets.contains(pet));
    }

    @Test(dependsOnMethods = "getPetByIdAndDoCheck")
    public void updateExistingPet() {

        pet.setName("MyLittlePet_" + RandomStringUtils.randomAlphanumeric(5));
        pet.setStatus(StatusEnum.pending);

        Assert.assertEquals(given(requestSpecification)
                .body(pet)
                .put(PET_ENDPOINT)
                .as(Pet.class), pet);

    }
}
